define({ "api": [
  {
    "type": "post",
    "url": "/v1/shipping/carrier",
    "title": "Agregar Proveedor de Envio",
    "name": "Agregar_Proveedor_de_Envio",
    "group": "Carriers",
    "description": "<p>Se encarga de la creación de un proveedor de envío. Esta acción podrá ser realizada solo un usuario con rol admin.</p>",
    "examples": [
      {
        "title": "Body",
        "content": "{\n  \"name\": \"{nombre del método}\",\n  \"prices\": {\n             \"short_distance\" :  {Precio corta distancia},\n             \"medium_distance\" :  {Precio media distancia},\n             \"long_distance\" :  {Precio larga distancia}\n          }\n}",
        "type": "json"
      },
      {
        "title": "Header Autorización",
        "content": "Authorization=bearer {token}",
        "type": "json"
      }
    ],
    "success": {
      "examples": [
        {
          "title": "200 Respuesta",
          "content": "HTTP/1.1 200 Ok\n {\n     \"id\": {id del metodo}\n     \"name\": \"{nombre del metodo}\",\n     \"prices\": {\n             {\"corta_distancia\" :  {Precio corta distancia}},\n             {\"media_distancia\" :  {Precio media distancia}},\n             {\"larga_distancia\" :  {Precio larga distancia}}\n         }\n     \"updated\": {fecha ultima actualización}\n     \"created\": {fecha creación}\n }",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "401 Unauthorized",
          "content": "HTTP/1.1 401 Unauthorized",
          "type": "json"
        },
        {
          "title": "500 Server Error",
          "content": "HTTP/1.1 500 Internal Server Error\n{\n   \"error\" : \"Not Found\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/Domain/Shipping/crud_service.php",
    "groupTitle": "Carriers"
  },
  {
    "type": "delete",
    "url": "v1/shipping/carrier/:carrier_id",
    "title": "Eliminar Proveedor de Envio",
    "name": "Eliminar_Proveedor_de_Envio",
    "group": "Carriers",
    "description": "<p>Se encarga de la eliminación de un proveedor de envío del sistema.</p>",
    "examples": [
      {
        "title": "Header Autorización",
        "content": "Authorization=bearer {token}",
        "type": "json"
      }
    ],
    "success": {
      "examples": [
        {
          "title": "200 Respuesta",
          "content": "HTTP/1.1 200 Ok",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/Domain/Shipping/crud_service.php",
    "groupTitle": "Carriers",
    "error": {
      "examples": [
        {
          "title": "400 Bad Request",
          "content": "HTTP/1.1 400 Bad Request\n{\n   \"path\" : \"{Nombre de la propiedad}\",\n   \"message\" : \"{Motivo del error}\"\n}",
          "type": "json"
        },
        {
          "title": "500 Server Error",
          "content": "HTTP/1.1 500 Internal Server Error\n{\n   \"error\" : \"Not Found\"\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "type": "get",
    "url": "/v1/shipping/carriers",
    "title": "Listar Proveedores de Envio",
    "name": "Listar_Proveedores_de_Envio",
    "group": "Carriers",
    "description": "<p>Busca todos los proveedores de envio existentes en el sistema.</p>",
    "success": {
      "examples": [
        {
          "title": "200 Respuesta",
          "content": "HTTP/1.1 200 Ok\n [\n    {Proveedores de envio}\n ]",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/Domain/Shipping/crud_service.php",
    "groupTitle": "Carriers",
    "error": {
      "examples": [
        {
          "title": "500 Server Error",
          "content": "HTTP/1.1 500 Internal Server Error\n{\n   \"error\" : \"Not Found\"\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "type": "post",
    "url": "/v1/shipping/carrier/:carrier_id",
    "title": "Modificar Proveedor de Envio",
    "name": "Modificar_Proveedor_de_Envio",
    "group": "Carriers",
    "description": "<p>Se encarga de la modificación del proveedor de envío especificado. Esta acción podrá ser realizada solo un usuario con rol admin.</p>",
    "examples": [
      {
        "title": "Body",
        "content": "{\n  \"name\": \"{nombre del método}\",\n  \"prices\": {\n             \"short_distance\" :  {Precio corta distancia},\n             \"medium_distance\" :  {Precio media distancia},\n             \"long_distance\" :  {Precio larga distancia}\n          }\n}",
        "type": "json"
      },
      {
        "title": "Header Autorización",
        "content": "Authorization=bearer {token}",
        "type": "json"
      }
    ],
    "success": {
      "examples": [
        {
          "title": "200 Respuesta",
          "content": "HTTP/1.1 200 Ok\n {\n     \"id\": {id del metodo}\n     \"name\": \"{nombre del metodo}\",\n     \"prices\": {\n             {\"corta_distancia\" :  {Precio corta distancia}},\n             {\"media_distancia\" :  {Precio media distancia}},\n             {\"larga_distancia\" :  {Precio larga distancia}}\n              }\n     \"updated\": {fecha ultima actualización}\n     \"created\": {fecha creación}\n }",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/Domain/Shipping/crud_service.php",
    "groupTitle": "Carriers",
    "error": {
      "examples": [
        {
          "title": "400 Bad Request",
          "content": "HTTP/1.1 400 Bad Request\n{\n   \"path\" : \"{Nombre de la propiedad}\",\n   \"message\" : \"{Motivo del error}\"\n}",
          "type": "json"
        },
        {
          "title": "500 Server Error",
          "content": "HTTP/1.1 500 Internal Server Error\n{\n   \"error\" : \"Not Found\"\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "type": "direct",
    "url": "v1/shipping/create-shipping",
    "title": "Crear envío.",
    "group": "RabbitMQ_GET",
    "description": "<p>Escucha la confirmación de una orden de compra con un proveedor de envío seleccionado para una distancia especificada por el usuario.</p>",
    "success": {
      "examples": [
        {
          "title": "Mensaje",
          "content": "{\n     \"type\": \"order-placed\",\n     \"exchange\": \"topic_shipping\",\n     \"queue\": \"topic_shipping\",\n     \"message\": {\n             \"carrier_name\": \"{Nombre del proveedor de envío}\",\n             \"order_id\": {Numero de orden}\n             \"distance\": {distancia del envío},\n         }\n }",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/Utility/RabbitService.php",
    "groupTitle": "RabbitMQ_GET",
    "name": "DirectV1ShippingCreateShipping"
  },
  {
    "type": "fanout",
    "url": "auth/logout",
    "title": "Logout.",
    "group": "RabbitMQ_GET",
    "description": "<p>Escucha de mensajes logout desde auth. Invalida sesiones en cache.</p>",
    "success": {
      "examples": [
        {
          "title": "Mensaje",
          "content": "{\n     \"type\": \"article-exist\",\n     \"message\" : \"tokenId\"\n }",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/Utility/Security.php",
    "groupTitle": "RabbitMQ_GET",
    "name": "FanoutAuthLogout"
  },
  {
    "type": "direct",
    "url": "/v1/notification/update-shipping",
    "title": "Notificación del estado del envío.",
    "group": "RabbitMQ_POST",
    "description": "<p>Envía mensajes al servicio de notificaciones para informar al usuario acerca del estado de envío de su pedido.</p>",
    "success": {
      "examples": [
        {
          "title": "Mensaje",
          "content": "{\n     \"type\": \"update-shipping\"\n     \"message\": {\n             \"shipping_id\": {Id del envío}\n             \"status\": \"{Nombre del estado}\"\n         }\n }",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/Utility/RabbitService.php",
    "groupTitle": "RabbitMQ_POST",
    "name": "DirectV1NotificationUpdateShipping"
  },
  {
    "type": "post",
    "url": "/v1/shipping/status",
    "title": "Actualizar estado de envio",
    "name": "Actualizar_estado_de_envio_",
    "group": "Shipping",
    "description": "<p>Permite la actualización del estado del envío(Este caso de uso facilita un endpoint para recibir la actualización por parte del sistema del proveedor de envío).</p>",
    "examples": [
      {
        "title": "Body",
        "content": "{\n  \"reference_id\": {Id de referencia del proveedor},\n  \"status\": {\"Despachado\" | \"Enviado\" | \"Recibido\"}\n}",
        "type": "json"
      }
    ],
    "success": {
      "examples": [
        {
          "title": "200 Respuesta",
          "content": "HTTP/1.1 200 Ok",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/Domain/Shipping/shipping_service.php",
    "groupTitle": "Shipping",
    "error": {
      "examples": [
        {
          "title": "400 Bad Request",
          "content": "HTTP/1.1 400 Bad Request\n{\n   \"path\" : \"{Nombre de la propiedad}\",\n   \"message\" : \"{Motivo del error}\"\n}",
          "type": "json"
        },
        {
          "title": "500 Server Error",
          "content": "HTTP/1.1 500 Internal Server Error\n{\n   \"error\" : \"Not Found\"\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "type": "get",
    "url": "/v1/shipping/:shipping_id",
    "title": "Consultar información de envío.",
    "name": "Consultar_informaci_n_de_env_o_",
    "group": "Shipping",
    "description": "<p>Permite que un usuario pueda consultar la información del envío de su orden.</p>",
    "success": {
      "examples": [
        {
          "title": "200 Respuesta",
          "content": "HTTP/1.1 200 Ok\n {\n    \"status\": {\"Despachado\" | \"Enviado\" | \"Recibido\"}\n    \"distancia\": {distancia de envio},\n    \"precio\": {precio del envio}\n  }",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/Domain/Shipping/shipping_service.php",
    "groupTitle": "Shipping",
    "error": {
      "examples": [
        {
          "title": "400 Bad Request",
          "content": "HTTP/1.1 400 Bad Request\n{\n   \"path\" : \"{Nombre de la propiedad}\",\n   \"message\" : \"{Motivo del error}\"\n}",
          "type": "json"
        },
        {
          "title": "500 Server Error",
          "content": "HTTP/1.1 500 Internal Server Error\n{\n   \"error\" : \"Not Found\"\n}",
          "type": "json"
        }
      ]
    }
  }
] });
