# Microservicio de Envios y Proveedores de Envios

Este microservicio consiste en la gestión de los proveedores de envío, para realizar este, de algún producto luego de que se realiza la orden de los mismos. Esta gestión consiste en listar los métodos de envío, con sus precios, dependiendo de la distancia a enviar el producto. 
El usuario con rol administrador debe poder modificar un proveedor de envío, como también agregar uno nuevo o eliminar uno existente.
El usuario con rol cliente debe poder obtener información de todos los métodos de envío disponibles en el caso que simplemente quiera conocer de ellos.
Luego de la creación de la orden de compra, el microservicio se debe encargar de crear un envío correspondiente a la elección del usuario e informa acerca de los cambio en el estado de ese envío al microservicio de notificaciones.

[Documentación de API](./README-API.md)

La documentación de las api también se pueden consultar desde el home del microservicio
que una vez levantado el servidor se puede navegar en [localhost:8080](http://localhost:8080/)

## Dependencias

### Auth

El servicio de proveedores de envio sólo puede usarse por usuario autenticados, algunas operaciones como agregar un proveedor nuevo requieren que el usuario sea "admin".

### Order

El servicio de shipping utiliza el servicio de order para escuchar cuando una orden es creada y asi crear el envio para esa orden.

### RabbitMQ

La comunicación con Shipping, Auth y  Order es a través de rabbit.

Ver tutorial de instalación en [README.md](../README.md) en la raíz.

## Shipping

Para usar este microservicio, se debe:

1. Descargar [Composer](https://getcomposer.org/doc/00-intro.md), si es que no esta instalado.
2. Instalar las dependecias "composer install".
3. Hacer las migraciones "bin/cake migrations migrate".
4. Ejecutar "bin/cake bin/cake server -p 8080" para ejecutar el servicio.
5. Ejecutar "bin/cake rabbitmq", en otra consola, para ejecutar el servicio de rabbit para este servicio.

## Apidoc

Apidoc es una herramienta que genera documentación de apis para proyectos node (ver [Apidoc](http://apidocjs.com/)).
Este proyecto utiliza apidoc para documentar los servicios rest.

El microservicio muestra la documentación como archivos estáticos si se abre en un browser la raíz del servidor [localhost:8080](http://localhost:8080/)

Ademas se genera la documentación en formato markdown.

Para que funcione correctamente hay que instalarla globalmente con npm

```bash
npm install -g apidoc
npm install -g apidoc-markdown2
```

La documentación ya se encuentra creada en webroot/public/apiDoc

