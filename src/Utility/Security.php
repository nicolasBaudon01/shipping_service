<?php
namespace App\Utility;

use Cake\Http\Client;
use Cake\Core\Configure;
use App\Utility\Config;
use Cake\Cache\Cache;

class Security{
    public static function isValidToken($bearerToken = null){
        if (Cache::read($bearerToken)) {
            $profile = Cache::read($bearerToken);
            return $profile;
        }else{
            $headers = ["Authorization" => $bearerToken];
            $http = new Client(["headers" => $headers]);
            $authUrl = Config::getAuthUrl();
            $response = $http->get($authUrl . '/v1/users/current');
            if($response->getStatusCode() != 200){
                echo 'HTTP/1.1 401 Unauthorized';
                header('HTTP/1.1 401 Unauthorized');
                exit;
            }else{
                Cache::write($bearerToken, $response->getJson());
                return $response->getJson();
            }
        }
    }
    public static function isAdminRole($bearerToken = null){
        $profile = Security::isValidToken($bearerToken);
        if(in_array('admin', $profile['permissions'])){
            return;
        }else{
            echo 'Insufficient access level';
            echo 'HTTP/1.1 401 Unauthorized';
            header('HTTP/1.1 401 Unauthorized');
            exit;
        }
    }
    public static function logout($message){
        /**
         * @api {fanout} auth/logout Logout.
         * @apiGroup RabbitMQ GET
         *
         * @apiDescription Escucha de mensajes logout desde auth. Invalida sesiones en cache. 
         *
         * @apiSuccessExample {json} Mensaje
         *      {
         *           "type": "article-exist",
         *           "message" : "tokenId"
         *       }
         *
         */
        $message = utf8_decode($message->body);
        $message = json_decode($message);
        $message = $message->message;
        if(Cache::read($message)){
            Cache::delete($message);
            echo 'Sesion invalida';
            return 0;
        }else{
            return 0;
        }
    }
}