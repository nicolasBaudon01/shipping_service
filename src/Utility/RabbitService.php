<?php
namespace App\Utility;

require __DIR__ .'/../../vendor/autoload.php';
use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;
use RabbitMQ\CakephpRabbitMQ as MQ;
use Cake\ORM\TableRegistry;
use Cake\Core\Configure;

class RabbitService{

    public function __construct()
    {
    }

    public static function init(){
        // Inicia los servicios de RabbitMQ
        RabbitService::initOrder();
    }

    public static function initOrder(){
        // Inicializa RabbitMQ para escuchar eventos de order específicos.
         /**
         * @api {direct} v1/shipping/create-shipping Crear envío.
         * @apiGroup RabbitMQ GET
         *
         * @apiDescription Escucha la confirmación de una orden de compra con un proveedor de envío seleccionado para una distancia especificada por el usuario. 
         *
         * @apiSuccessExample {json} Mensaje
         *      {
         *           "type": "order-placed",
         *           "exchange": "topic_shipping",
         *           "queue": "topic_shipping",
         *           "message": {
         *                   "carrier_name": "{Nombre del proveedor de envío}",
         *                   "order_id": {Numero de orden}
         *                   "distance": {distancia del envío},
         *               }
         *       }
         *
         */
        // En este metodo se deberia iniciar el 
        // servicio de escucha pero debido al framework 
        // se ejecuta como otro servicio desde la consola de comandos
    }

    public static function createShipping($message){
        require_once(ROOT. DS . 'src' . DS . 'Domain' . DS . 'Shipping' . DS . 'crud_service.php');
        $result = \CrudService::createShipping(utf8_decode($message->body));
        return 0;
    }

    private function sendStatusMessage($message = null){
        try {
            $connection = new AMQPStreamConnection('localhost', 5672, 'guest', 'guest');
            $channel = $connection->channel();
            $channel->exchange_declare('shipping', 'direct', false, false, false);
            $channel->queue_declare('update-shipping', false, false, false, false);
            $channel->queue_bind('update-shipping', 'shipping', 'update-shipping');
            $msg = new AMQPMessage(json_encode($message));
            $channel->basic_publish($msg, 'shipping', 'update-shipping');
            $channel->close();
            $connection->close();
        } catch (\Throwable $th) {

        }
    }
    public static function sendStatus($shipping_id = null, $shipping_status = null){
     /**
     * @api {direct} /v1/notification/update-shipping Notificación del estado del envío.
     * @apiGroup RabbitMQ POST
     *
     * @apiDescription Envía mensajes al servicio de notificaciones para informar al usuario acerca del estado de envío de su pedido. 
     *
     * @apiSuccessExample {json} Mensaje
     *      {
     *           "type": "update-shipping"
     *           "message": {
     *                   "shipping_id": {Id del envío}
     *                   "status": "{Nombre del estado}"
     *               }
     *       }
     *
     */
        return RabbitService::sendStatusMessage([
            'type' => 'update-shipping',
            'message' => [
                'shipping_id' => $shipping_id,
                'status' => $shipping_status
            ]
        ]);
    }
}

