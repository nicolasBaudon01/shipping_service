<?php
namespace App\Controller;

use App\Controller\AppController;
use App\Utility\Security;

class CarriersController extends AppController
{
    public function index()
    {
        $this->autorender = false;
        require_once(ROOT. DS . 'src' . DS . 'Domain' . DS . 'Shipping' . DS . 'crud_service.php');
        $result = \CrudService::getCarriers();
        if($result['success'] == true){
            $this->response->type('json');
            $this->response->body(json_encode($result['data']));
            return $this->response;
        }else{
            if($result['errors']['code'] == 500){
                $this->response->type('json');
                $this->response->statusCode(500);
                $this->response->body(json_encode($result['errors']['data']));
                return $this->response;
            }
        }
    }

    public function add()
    {
        $this->autorender = false;
        require_once(ROOT. DS . 'src' . DS . 'Domain' . DS . 'Shipping' . DS . 'crud_service.php');
        Security::isAdminRole($this->request->header('Authorization'));
        $body = $this->request->getData();
        $result = \CrudService::add($body);
        if($result['success'] == true){
            $this->response->type('json');
            $this->response->body(json_encode($result['data']));
            return $this->response;
        }else{
            if($result['errors']['code'] == 500){
                $this->response->type('json');
                $this->response->statusCode(500);
                $this->response->body(json_encode($result['errors']['data']));
                return $this->response;
            }
        }
    }

    public function edit()
    {
        $this->autorender = false;
        require_once(ROOT. DS . 'src' . DS . 'Domain' . DS . 'Shipping' . DS . 'crud_service.php');
        Security::isAdminRole($this->request->header('Authorization'));
        $body = $this->request->getData();
        $body['carrier_id'] = $this->request->getParam('carrier_id');
        $result = \CrudService::edit($body);
        if($result['success'] == true){
            $this->response->type('json');
            $this->response->body(json_encode($result['data']));
            return $this->response;
        }else{
            if($result['errors']['code'] == 500){
                $this->response->type('json');
                $this->response->statusCode(500);
                $this->response->body(json_encode($result['errors']['data']));
                return $this->response;
            }else{
                $this->response->type('json');
                $this->response->statusCode(400);
                $this->response->body(json_encode($result['errors']['data']));
                return $this->response;
            }
        }
    }

    public function delete()
    {
        $this->autorender = false;
        require_once(ROOT. DS . 'src' . DS . 'Domain' . DS . 'Shipping' . DS . 'crud_service.php');
        Security::isAdminRole($this->request->header('Authorization'));
        $body = $this->request->getData();
        $body['carrier_id'] = $this->request->getParam('carrier_id');
        $result = \CrudService::delete($body);
        if($result['success'] == true){
            $this->response->type('json');
            $this->response->body(json_encode($result['data']));
            return $this->response;
        }else{
            if($result['errors']['code'] == 500){
                $this->response->type('json');
                $this->response->statusCode(500);
                $this->response->body(json_encode($result['errors']['data']));
                return $this->response;
            }else{
                $this->response->type('json');
                $this->response->statusCode(400);
                $this->response->body(json_encode($result['errors']['data']));
                return $this->response;
            }
        }
    }
}
