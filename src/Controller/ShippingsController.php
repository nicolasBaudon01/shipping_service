<?php
namespace App\Controller;

use App\Controller\AppController;
use App\Utility\Security;

class ShippingsController extends AppController
{
    public function index()
    {
        $this->autorender = false;
        require_once(ROOT. DS . 'src' . DS . 'Domain' . DS . 'Shipping' . DS . 'shipping_service.php');
        $body = $this->request->getData();
        $body['shipping_id'] = $this->request->getParam('shipping_id');
        $result = \ShippingService::getShipping($body);
        if($result['success'] == true){
            $this->response->type('json');
            $this->response->body(json_encode($result['data']));
            return $this->response;
        }else{
            if($result['errors']['code'] == 500){
                $this->response->type('json');
                $this->response->statusCode(500);
                $this->response->body(json_encode($result['errors']['data']));
                return $this->response;
            }else{
                $this->response->type('json');
                $this->response->statusCode(400);
                $this->response->body(json_encode($result['errors']['data']));
                return $this->response;
            }
        }
    }
    public function edit()
    {
        $this->autorender = false;
        require_once(ROOT. DS . 'src' . DS . 'Domain' . DS . 'Shipping' . DS . 'shipping_service.php');
        $body = $this->request->getData();
        $result = \ShippingService::updateStatus($body);
        if($result['success'] == true){
            $this->response->type('json');
            $this->response->body(json_encode($result['data']));
            return $this->response;
        }else{
            if($result['errors']['code'] == 500){
                $this->response->type('json');
                $this->response->statusCode(500);
                $this->response->body(json_encode($result['errors']['data']));
                return $this->response;
            }else{
                $this->response->type('json');
                $this->response->statusCode(400);
                $this->response->body(json_encode($result['errors']['data']));
                return $this->response;
            }
        }
    }
}
