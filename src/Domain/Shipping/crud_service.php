<?php 
use Cake\ORM\TableRegistry;

class CrudService{
    //Crud service de Carriers
    public function __construct()
    {
    }
    public static function getCarriers()
    {
    /**
     * @api {get} /v1/shipping/carriers Listar Proveedores de Envio
     * @apiName Listar Proveedores de Envio
     * @apiGroup Carriers
     *
     * @apiDescription Busca todos los proveedores de envio existentes en el sistema.
     * 
     * @apiSuccessExample {json} 200 Respuesta
     *     HTTP/1.1 200 Ok
     *      [
     *         {Proveedores de envio}
     *      ]
     * 
     * @apiUse OtherErrors
     * 
     *
     */
        try {
            $response['success'] = true;
            $carriers_table = TableRegistry::get('Carriers');
            $carriers = $carriers_table->find('all')->contain(['Prices']);
            if($carriers->count() > 0){
                $response['data'] = $carriers;
                return $response;
            }
        } catch (\Throwable $th) {
            $response['success'] = false;
            $response['errors']['code'] = 500;
            $response['errors']['data'] = ['error' => $th->getMessage()];
            return $response;
        }
    }
    public static function add($body = null)
    {
    /**
     * @api {post} /v1/shipping/carrier Agregar Proveedor de Envio
     * @apiName Agregar Proveedor de Envio
     * @apiGroup Carriers
     *
     * @apiDescription Se encarga de la creación de un proveedor de envío. Esta acción podrá ser realizada solo un usuario con rol admin.
     *
     * @apiExample {json} Body
     *    {
     *      "name": "{nombre del método}",
     *      "prices": {
     *                 "short_distance" :  {Precio corta distancia},
     *                 "medium_distance" :  {Precio media distancia},
     *                 "long_distance" :  {Precio larga distancia}
     *              }
     *    }
     * 
     * @apiExample {json} Header Autorización
     *    Authorization=bearer {token}
     * 
     * @apiSuccessExample {json} 200 Respuesta
     *     HTTP/1.1 200 Ok
     *      {
     *          "id": {id del metodo}
     *          "name": "{nombre del metodo}",
     *          "prices": {
     *                  {"corta_distancia" :  {Precio corta distancia}},
     *                  {"media_distancia" :  {Precio media distancia}},
     *                  {"larga_distancia" :  {Precio larga distancia}}
     *              }
     *          "updated": {fecha ultima actualización}
     *          "created": {fecha creación}
     *      }
     * 
     * @apiErrorExample 401 Unauthorized
     *     HTTP/1.1 401 Unauthorized
     * 
     * @apiUse OtherErrors
     * 
     *
     */
        try{
            if($body != null){
                $response['success'] = true;
                $carriers_table = TableRegistry::get('Carriers');
                $prices_table = TableRegistry::get('Prices');
                $carrier_schema = ['name' => $body['name']];
                $carrier = $carriers_table->newEntity($carrier_schema);  
                if(!($carrier = $carriers_table->save($carrier))){
                    return json_encode([$carrier->errors()]);
                }
                $prices_entities = [];
                foreach($body['prices'] as $price_name => $price){
                    $price_schema = [
                                    'name' => $price_name, 
                                    'price' => $price, 
                                    'carrier_id' => $carrier['id']
                                    ];
                    $new_price = $prices_table->newEntity($price_schema);
                    array_push($prices_entities, $new_price);
                }
                if($prices_entities && $result = $prices_table->saveMany($prices_entities)){
                    $response['data']['id'] = $carrier['id'];
                    $response['data']['name'] = $carrier['name'];
                    $response['data']['prices'] = [];
                    foreach($result as $price){
                        array_push($response['data']['prices'], [$price->name => $price->price]);
                    }
                    $response['data']['created'] = $carrier['created'];
                    $response['data']['updated'] = $carrier['modified'];
                    return $response;
                }else{
                    $response['success'] = false;
                    $response['errors']['code'] = 500;
                    $response['errors']['data'] = ['error' => $prices_entities->errors()];
                    return $response;
                }
            }else{
                $response['success'] = false;
                $response['errors']['code'] = 500;
                $response['errors']['data'] = ['error' => 'Null Body'];
                return $response;
            }
        }catch (\Throwable $th) {
            $response['success'] = false;
            $response['errors']['code'] = 500;
            $response['errors']['data'] = ['error' => $th->getMessage()];
            return $response;
        }
    }

    public static function edit($body = null)
    {
    /**
     * @api {post} /v1/shipping/carrier/:carrier_id Modificar Proveedor de Envio
     * @apiName Modificar Proveedor de Envio
     * @apiGroup Carriers
     *
     * @apiDescription Se encarga de la modificación del proveedor de envío especificado. Esta acción podrá ser realizada solo un usuario con rol admin.
     *
     * @apiExample {json} Body
     *    {
     *      "name": "{nombre del método}",
     *      "prices": {
     *                 "short_distance" :  {Precio corta distancia},
     *                 "medium_distance" :  {Precio media distancia},
     *                 "long_distance" :  {Precio larga distancia}
     *              }
     *    }
     * 
     * @apiSuccessExample {json} 200 Respuesta
     *     HTTP/1.1 200 Ok
     *      {
     *          "id": {id del metodo}
     *          "name": "{nombre del metodo}",
     *          "prices": {
     *                  {"corta_distancia" :  {Precio corta distancia}},
     *                  {"media_distancia" :  {Precio media distancia}},
     *                  {"larga_distancia" :  {Precio larga distancia}}
     *                   }
     *          "updated": {fecha ultima actualización}
     *          "created": {fecha creación}
     *      }
     * 
     * @apiExample {json} Header Autorización
     *    Authorization=bearer {token}
     *
     * @apiUse ParamValidationErrors
     * @apiUse OtherErrors
     * 
     *
     */
        try {
            if($body != null){
                $response['success'] = true;
                $carriers_table = TableRegistry::get('Carriers');
                $prices_table = TableRegistry::get('Prices');
                $carrier = $carriers_table->find('all')->where(['id' => $body['carrier_id']])->contain(['Prices'])->first();  
                if(!($carrier)){
                    $response['success'] = false;
                    $response['errors']['code'] = 400;
                    $response['errors']['data'] = ['path' => '/v1/shipping/carrier/'.$body['carrier_id'], 'message' => 'Invalid carrier id'];
                    return $response;
                }
                if(isset($body['name'])){
                    $carrier['name'] = $body['name'];
                }
                $prices_entities = [];
                if(isset($body['prices'])){
                    foreach($body['prices'] as $price_name => $price){
                        $is_new = true;
                        foreach($carrier['prices'] as $carrier_price){
                            if($price_name == $carrier_price->name){
                                $carrier_price->price = $price;
                                array_push($prices_entities, $carrier_price);
                                $is_new = false;
                            }
                        }
                        if($is_new == true){
                            $price_schema = [
                                            'name' => $price_name, 
                                            'price' => $price, 
                                            'carrier_id' => $carrier['id']
                                            ];
                            $new_price = $prices_table->newEntity($price_schema);
                            array_push($prices_entities, $new_price);
                        }
                    }
                }
                if($carrier_result = $carriers_table->save($carrier)){
                    if($prices_entities && $prices_result = $prices_table->saveMany($prices_entities)){
                        $carrier = $carriers_table->find('all')->where(['id' => $carrier_result['id']])->contain(['Prices'])->first();
                        $response['data']['id'] = $carrier['id'];
                        $response['data']['name'] = $carrier['name'];
                        $response['data']['prices'] = [];
                        foreach($carrier['prices'] as $price){
                            array_push($response['data']['prices'], [$price->name => $price->price]);
                        }
                        $response['data']['created'] = $carrier['created'];
                        $response['data']['updated'] = $carrier['modified'];
                        return $response;
                    }else{
                        $response['success'] = false;
                        $response['errors']['code'] = 500;
                        $response['errors']['data'] = ['error' => $prices_entities->errors()];
                        return $response;
                    }
                }else{
                    $response['success'] = false;
                    $response['errors']['code'] = 500;
                    $response['errors']['data'] = ['error' => $carrier->errors()];
                    return $response;
                };
            }else{
                $response['success'] = false;
                $response['errors']['code'] = 500;
                $response['errors']['data'] = ['error' => 'Null Body'];
                return $response;
            }
        } catch (\Throwable $th) {
            $response['success'] = false;
            $response['errors']['code'] = 500;
            $response['errors']['data'] = ['error' => $th->getMessage()];
            return $response;
        }
    }

    public static function delete($body = null)
    {
        /**
        * @api {delete} v1/shipping/carrier/:carrier_id Eliminar Proveedor de Envio
        * @apiName Eliminar Proveedor de Envio
        * @apiGroup Carriers
        *
        * @apiDescription Se encarga de la eliminación de un proveedor de envío del sistema.
        *
        * @apiExample {json} Header Autorización
        *    Authorization=bearer {token}
        * 
        * @apiSuccessExample {json} 200 Respuesta
        *    HTTP/1.1 200 Ok
        *
        * @apiUse ParamValidationErrors
        * @apiUse OtherErrors
        */
        try {
            $response['success'] = true;
            $carriers_table = TableRegistry::get('Carriers');
            $prices_table = TableRegistry::get('Prices');
            if($carrier = $carriers_table->find('all')->where(['id' => $body['carrier_id']])->contain(['Prices'])->first()){
                foreach($carrier['prices'] as $price){
                    $prices_table->delete($price);
                }
                $carriers_table->delete($carrier);
                $response['data'] = 'HTTP/1.1 200 OK';
                return $response;
            }else{
                $response['success'] = false;
                $response['errors']['code'] = 400;
                $response['errors']['data'] = ['path' => '/v1/shipping/carrier/'.$body['carrier_id'], 'message' => 'Invalid carrier id'];
                return $response;
            }
        } catch (\Throwable $th) {
            $response['success'] = false;
            $response['errors']['code'] = 500;
            $response['errors']['data'] = ['error' => $th->getMessage()];
            return $response;
        }
    }

    public static function createShipping($body = null){
        $body = json_decode($body);
        $body = $body->message;
        $price_id = 0;
        $shippings_table = TableRegistry::get('Shippings');
        $carriers_table = TableRegistry::get('Carriers');
        $carrier = $carriers_table->find('all', ['contain' => ['Prices']])->where(['Carriers.name' => $body->carrier])->first();
        foreach($carrier['prices'] as $price){
            if($price['name'] == $body->distance){
                $price_id = $price['id'];
            }
        }
        $prices_table = TableRegistry::get('Prices');
        $shipping_schema = ['reference_id' => 1234567, 
                            'status' => 'Despachado',
                            'price_id' => $price_id
                            ];
        $shipping = $shippings_table->newEntity($shipping_schema);
        $shipping = $shippings_table->save($shipping);
        echo 'Shipping Created - Order '.$body->orderId.' processed';
        return $shipping;
    }
}