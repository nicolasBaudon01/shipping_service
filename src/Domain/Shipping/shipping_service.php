<?php 
use Cake\ORM\TableRegistry;
use App\Utility\RabbitService;

class ShippingService{
    public static function getShipping($body = null){
        /**
         * @api {get} /v1/shipping/:shipping_id Consultar información de envío.
         * @apiName Consultar información de envío.
         * @apiGroup Shipping
         *
         * @apiDescription Permite que un usuario pueda consultar la información del envío de su orden.
         *
         * 
         * @apiSuccessExample {json} 200 Respuesta
         *     HTTP/1.1 200 Ok
         *      {
         *         "status": {"Despachado" | "Enviado" | "Recibido"}
         *         "distancia": {distancia de envio},
         *         "precio": {precio del envio}
         *       }
         * 
         * @apiUse ParamValidationErrors
         * @apiUse OtherErrors
         * 
         *
         */
        try {
            $response['success'] = true;
            $shippings_table = TableRegistry::get('Shippings');
            $shipping = $shippings_table->find('all')->where(['Shippings.id' => $body['shipping_id']])->contain(['Prices'])->first();
            if($shipping){
                $response['data']['status'] = $shipping['status'];
                $response['data']['distance'] = $shipping['price']['name'];
                $response['data']['price'] = $shipping['price']['price'];
                return $response;
            }else{
                $response['success'] = false;
                $response['errors']['code'] = 400;
                $response['errors']['data'] = ['path' => 'v1/shipping/'.$body['shipping_id'], 'message' => 'Invalid shipping id'];
                return $response;
            }
        } catch (\Throwable $th) {
            $response['success'] = false;
            $response['errors']['code'] = 500;
            $response['errors']['data'] = ['error' => $th->getMessage()];
            return $response;
        }
    }

    public static function updateStatus($body = null){
        /**
         * @api {post} /v1/shipping/status Actualizar estado de envio
         * @apiName Actualizar estado de envio.
         * @apiGroup Shipping
         *
         * @apiDescription Permite la actualización del estado del envío(Este caso de uso facilita un endpoint para recibir la actualización por parte del sistema del proveedor de envío).
         *
         * @apiExample {json} Body
         *    {
         *      "reference_id": {Id de referencia del proveedor},
         *      "status": {"Despachado" | "Enviado" | "Recibido"}
         *    }
         * 
         * @apiSuccessExample {json} 200 Respuesta
         *    HTTP/1.1 200 Ok
         * 
         * @apiUse ParamValidationErrors
         * @apiUse OtherErrors
         * 
         *
         */
        try {
            if($body != null){
                $response['success'] = true;
                $shippings_table = TableRegistry::get('Shippings');
                $shipping = $shippings_table->find('all')->where(['Shippings.reference_id' => $body['reference_id']])->first();
                if($shipping){
                    $shipping['status'] = $body['status'];
                    if($result = $shippings_table->save($shipping)){
                        RabbitService::sendStatus($result['id'], $result['status']);
                        $response['data'] = 'HTTP/1.1 200 OK';
                        return $response;
                    }else{
                        $response['success'] = false;
                        $response['errors']['code'] = 500;
                        $response['errors']['data'] = ['error' => 'Database Saving Error'];
                        return $response;
                    }
                }else{
                    $response['success'] = false;
                    $response['errors']['code'] = 400;
                    $response['errors']['data'] = ['path' => '/v1/shipping/status', 'message' => 'Invalid reference id'];
                    return $response;
                }
            }
            $response['success'] = false;
            $response['errors']['code'] = 500;
            $response['errors']['data'] = ['error' => 'Null Body'];
            return $response;
        } catch (\Throwable $th) {
            $response['success'] = false;
            $response['errors']['code'] = 500;
            $response['errors']['data'] = ['error' => $th->getMessage()];
            return $response;
        }
    }
}