<?php
use Migrations\AbstractMigration;

class AddShippingReferenceId extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('shippings');
        $table->addColumn('reference_id', 'integer', [
            'default' => null,
            'null' => true,
        ])->update();
    }
}
