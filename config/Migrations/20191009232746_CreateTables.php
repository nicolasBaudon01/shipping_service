<?php
use Migrations\AbstractMigration;

class CreateTables extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        // Tabla Carrier
        $table = $this->table('carriers');
        $table->addColumn('name', 'string', [
            'default' => null,
            'limit' => 255,
            'null' => true,
        ]);
        $table->addColumn('deleted', 'datetime', [
            'default' => null,
            'null' => true,
        ]);
        $table->addColumn('created', 'datetime', [
            'default' => null,
            'null' => true,
        ]);
        $table->addColumn('modified', 'datetime', [
            'default' => null,
            'null' => true,
        ]);
        $table->create();
        // Tabla Prices
        $table = $this->table('prices');
        $table->addColumn('name', 'string', [
            'default' => null,
            'limit' => 255,
            'null' => true,
        ]);
        $table->addColumn('price', 'float', [
            'default' => null,
            'null' => true,
        ]);
        $table->addColumn('deleted', 'datetime', [
            'default' => null,
            'null' => true,
        ]);
        $table->addColumn('created', 'datetime', [
            'default' => null,
            'null' => true,
        ]);
        $table->addColumn('modified', 'datetime', [
            'default' => null,
            'null' => true,
        ]);
        $table->create();
        $refTable = $this->table('prices');
        $refTable-> addColumn('carrier_id', 'integer', array('signed'=>'disable'))
            ->addForeignKey('carrier_id', 'carriers', 'id', array('delete' => 'CASCADE', 'update' => 'NO_ACTION'))
            ->update();
        // Tabla Shipping
        $table = $this->table('shippings');
        $table->addColumn('status', 'string', [
            'default' => null,
            'limit' => 255,
            'null' => true,
        ]);
        $table->addColumn('deleted', 'datetime', [
            'default' => null,
            'null' => true,
        ]);
        $table->addColumn('created', 'datetime', [
            'default' => null,
            'null' => true,
        ]);
        $table->addColumn('modified', 'datetime', [
            'default' => null,
            'null' => true,
        ]);
        $table->create();
        $refTable = $this->table('shippings');
        $refTable-> addColumn('price_id', 'integer', array('signed'=>'disable'))
            ->addForeignKey('price_id', 'prices', 'id', array('delete' => 'CASCADE', 'update' => 'NO_ACTION'))
            ->update();
    }
}
